import React, { ReactElement } from 'react';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';
import { BrowserRouter } from 'react-router-dom';

export default function mockProvider<T>(
  WrapperComponent: React.FC<T>,
  initialState?: Record<string, unknown>,
): (props: T) => ReactElement {
  const mockStore = configureStore([]);

  const store = mockStore(initialState);
  return (props) => (
    <Provider store={store}>
      <BrowserRouter>
        <WrapperComponent {...props} />
      </BrowserRouter>
    </Provider>
  );
}
