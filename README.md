# React Marvel API

### O projeto tem por finalidade construir um front end para consumir a api da marvel (https://developer.marvel.com/docs), no projeto é possível consultar todos os personagens da marvel bem como ver as séries que cada personagem faz parte, também é possível editar mas somente para visualizar no lado do cliente

## Edição

### Na edição de um personagem só permitido alterar o nome e a foto, mas para alterar a fot deve ser atraves de uma url e com o nome da extenção no fim da imagem como por exemplo esse link https://s.aficionados.com.br/imagens/steve-rogers-0.jpg


## Tecnologias

- React
- Redux
- Redux-saga
- styled-componenets
- jest
- enzyme
- typescript
- commitlint
- eslint
- CI/CD
- Material ui


### Requisitos Funcionais:  

- [X] Crie uma lista de cards para exibir os personagens mostrando a imagem e o nome;
- [X] Possibilite o usuário buscar personagens em todos os personagens da base;
- [X] Na lista o usuário pode ir para a página de detalhes do personagem e ver a lista de series dele;
- [X] Crie um formulário para editar um personagem Marvel (salvando apenas no client-side);

### Requisitos não Funcionais:  

- [X] Usar React;
- [X] Utilize o create-react-app como base;
- [X] Utilize redux para gerenciar o estado;
- [X] Utilize react-router para trocar de página;
- [X] Utilize @testing-library/react para testes;

## Deploy

### Atualmente o projeto esta em dois ambientes no heroku
-   https://wyrvalski-marvel-api-dev.herokuapp.com/ ambiente de desenvolvimento
-   https://wyrvalski-marvel-api.herokuapp.com/ ambiente de produção

## Executar projeto local 

### Clonar projeto 

``git clone https://gitlab.com/eduardo.wyrvalski/marvel.git``

### Criar arquivo .env na raiz do projeto com as seguintes chaves
- `` REACT_APP_PUBLIC_KEY "o valor aqui deve ser da public key disponibilizada apos cadastro no link da api"``
- ``REACT_APP_PRIVATE_KEY="o valor aqui deve ser da private key disponibilizada apos cadastro no link da api"``
- ``REACT_APP_API_URL="https://gateway.marvel.com:443/v1/public/"``
- ``SKIP_PREFLIGHT_CHECK=true``

### instalar dependencias

 ``yarn``

#### Executar aplicação

``yarn start``

#### Executar Testes

``yarn test``

### Build

``yarn build``.

