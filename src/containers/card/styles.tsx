import styled from 'styled-components';

interface ImageProps {
  widthProps?: string;
  heightProps?: string;
}

export const CardComponent = styled.div`
  position: relative;
  padding: 10px;
  a {
    text-decoration: none;
    color: #111111;
  }
  :hover {
    position: relative;
    cursor: pointer;
    transform: scale(1.2, 1.2);
    transition: transform 0.5s;
  }
`;

export const Image = styled.img<ImageProps>`
  width: ${(props) => `${props.widthProps}`};
  height: ${(props) => `${props.heightProps}`};
`;
