import React, { ReactElement } from 'react';
import { Link } from 'react-router-dom';
import { CardComponent, Image } from './styles';
import { Character } from '../../services/interface';

const Card = ({ name, thumbnail, id }: Character): ReactElement => (
  <CardComponent>
    <Link to={`/character/info/${id}`}>
      <Image
        widthProps="200px"
        heightProps="200px"
        alt={name}
        src={`${thumbnail.path}.${thumbnail.extension}`}
      />
      <h3>{name}</h3>
    </Link>
  </CardComponent>
);
export default Card;
