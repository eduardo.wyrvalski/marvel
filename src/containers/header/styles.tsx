import styled from 'styled-components';

const HeaderComponent = styled.header`
  display: flex;
  justify-content: space-between;
  align-items: center;
  width: 100%;
  height: 60px;
  background-color: #FF6347;
  position: relative;

  h1 {
    padding: 10px;
    color: whitesmoke;

    :hover {
      cursor: default;
    }
  }

  nav {
    ul {
      display: flex;
      @media (max-width: 520px) {
        flex-direction: column;
        align-items: center;
        padding-inline-start: unset;
      }

      li {
        padding: 10px;
        flex-direction: row;
        list-style-type: none;

        a {
          text-decoration: none;

          :visited {
            color: #fdfcfc;
          }

          :hover {
            cursor: pointer;
            color: #8793fa;
          }
        }

      }
    }
  }

  @media (max-width: 520px) {
    flex-direction: column;
    height: 200px;
  }
`;

export default HeaderComponent;
