import 'jsdom-global/register';
import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import { mount } from 'enzyme';
import Header from '../index';

describe('tests app', () => {
  it('render app', () => {
    const wrapper = mount(
      <BrowserRouter>
        <Header />
      </BrowserRouter>,
    );
    expect(wrapper).toMatchSnapshot();
  });
});
