import React from 'react';
import { Link } from 'react-router-dom';
import HeaderComponent from './styles';

const Header: React.FC = () => (
  <>
    <HeaderComponent>
      <h1>Marvel</h1>
      <nav>
        <ul>
          <li>
            <Link to="/">Home</Link>
          </li>
        </ul>
      </nav>
    </HeaderComponent>
  </>
);

export default Header;
