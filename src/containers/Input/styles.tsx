import styled from 'styled-components';

const InputContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  p {
    padding: 5px;
  }
  input {
    height: 20px;
  }
`;

export default InputContainer;
