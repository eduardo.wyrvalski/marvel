import React, { ReactElement } from 'react';
import { UseFormRegisterReturn } from 'react-hook-form';
import InputContainer from './styles';

interface InputProps {
  type: string;
  label: string;
  register: UseFormRegisterReturn;
}

const Input = ({ type, label, register }: InputProps): ReactElement => (
  <InputContainer>
    <p>{label}</p>
    <input {...register} type={type} />
  </InputContainer>
);

export default Input;
