import styled from 'styled-components';

const LoaderContainer = styled.div`
  height: 800px;
  display: flex;
  align-items: center;
  justify-content: center;
`;

export default LoaderContainer;
