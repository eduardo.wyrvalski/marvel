import React, { ReactElement } from 'react';
import { CircularProgress } from '@material-ui/core';
import LoaderContainer from './styles';

const Loader = (): ReactElement => (
  <LoaderContainer>
    <CircularProgress disableShrink />
  </LoaderContainer>
);

export default Loader;
