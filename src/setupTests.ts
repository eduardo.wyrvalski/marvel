import 'regenerator-runtime/runtime';
import 'jsdom-global/register';
import '@testing-library/jest-dom';
import Adapter from '@wojtekmaj/enzyme-adapter-react-17';
import Enzyme from 'enzyme';
import { configure } from '@testing-library/dom';

Enzyme.configure({ adapter: new Adapter() });

configure({
  testIdAttribute: 'id',
});
