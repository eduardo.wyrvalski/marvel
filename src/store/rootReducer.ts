import { combineReducers } from 'redux';
import characters from '../services/characters/duck';

const rootReducer = combineReducers({
  characters,
});

export type Appstate = ReturnType<typeof rootReducer>;

export default rootReducer;
