import { createSelector } from 'reselect';

import { Appstate } from './rootReducer';

const selectedCharacter = (state: Appstate) => state.characters.characterInfo;

const loader = (state: Appstate) => state.characters.loading;

const seriesFromCharacterId = (state: Appstate) => state.characters.series;

const getSelectedCharacter = createSelector(
  selectedCharacter,
  (characterInfo) => characterInfo,
);

const getSeriesFromCharacterId = createSelector(
  seriesFromCharacterId,
  (series) => series,
);

const getLoader = createSelector(loader, (loading) => loading);

export default { getSelectedCharacter, getSeriesFromCharacterId, getLoader };
