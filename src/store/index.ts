import createSagaMiddleware from 'redux-saga';
import { persistStore } from 'redux-persist';
import rootSaga from './rootSaga';
import rootReducer from './rootReducer';
import createStore from './createStore';
import persistReducer from './persistReducer';
import history from './history';

const sagaMiddleware = createSagaMiddleware();

const store = createStore(persistReducer(rootReducer), sagaMiddleware, history);

const persistor = persistStore(store);

sagaMiddleware.run(rootSaga);

export type AppDispatch = typeof store.dispatch;

export { store, persistor };
