import { all } from 'redux-saga/effects';
import marvelSaga from './saga';

export default function* rootSaga(): Generator {
  yield all([marvelSaga()]);
}
