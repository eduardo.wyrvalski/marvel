import { createStore, applyMiddleware, Reducer, Store } from 'redux';
import { SagaMiddleware } from 'redux-saga';
import { composeWithDevTools } from 'redux-devtools-extension';
import { routerMiddleware } from 'connected-react-router';
import { History } from 'history';

export default (
  reducers: Reducer,
  sagaMiddleware: SagaMiddleware,
  history: History,
): Store => {
  const storeEnhacer = composeWithDevTools({ trace: true, traceLimit: 30 })(
    applyMiddleware(sagaMiddleware, routerMiddleware(history)),
  );
  return createStore(reducers, storeEnhacer);
};
