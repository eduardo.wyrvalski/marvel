import {
  call,
  takeLatest,
  put,
  CallEffect,
  PutEffect,
} from 'redux-saga/effects';
import { PayloadAction } from '@reduxjs/toolkit';
import { charactersActions } from '../services/characters/duck';
import {
  allCharacters,
  allSeriesByCharacterId,
  allSeriesFromCharacterId,
} from '../services/characters/characters';

function* getAllCharacters({
  payload: { page },
}: PayloadAction<{ page: number }>): Generator<CallEffect> | PutEffect {
  try {
    yield put(charactersActions.setLoader(true));
    const response = yield call(allCharacters, page);
    yield put(charactersActions.set(response?.data.results));
  } catch (errors) {
    throw new Error('Erro ao carregar os personagens.');
  } finally {
    yield put(charactersActions.setLoader(false));
  }
}

function* getAllSeriesFromCharacterId({
  payload: { characterId },
}: PayloadAction<{ characterId: string }>): Generator<CallEffect> | PutEffect {
  try {
    yield put(charactersActions.setLoader(true));
    const response = yield call(allSeriesFromCharacterId, characterId);
    yield put(charactersActions.setById(response.data?.results[0]));
  } catch (errors) {
    throw new Error('Erro ao carregar os personagens.');
  } finally {
    yield put(charactersActions.setLoader(false));
  }
}

function* getSerieById({
  payload: { characterId },
}: PayloadAction<{ characterId: string }>): Generator<CallEffect> | PutEffect {
  try {
    yield put(charactersActions.setLoader(true));
    const response = yield call(allSeriesByCharacterId, characterId);
    yield put(charactersActions.setSerieById(response.data?.results));
  } catch (errors) {
    throw new Error('Erro ao carregar os personagens.');
  } finally {
    yield put(charactersActions.setLoader(false));
  }
}

export default function* marvelSaga(): Generator {
  yield takeLatest(charactersActions.load.type, getAllCharacters);
  yield takeLatest(charactersActions.loadSerieById.type, getSerieById);
  yield takeLatest(
    charactersActions.loadById.type,
    getAllSeriesFromCharacterId,
  );
}
