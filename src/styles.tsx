import styled from 'styled-components';

const Main = styled.main`
  position: relative;
  display: grid;
  max-width: 1200px;
  align-content: center;
  margin: 40px auto;
  background-color: #d0c3c3ff;
  border-radius: 20px;
  color: whitesmoke;
  .pagination {
    height: 100px;
    display: flex;
    justify-content: center;
  }
`;

export default Main;
