import MockAdapter from 'axios-mock-adapter';
import { allCharacters, allSeriesByCharacterId, allSeriesFromCharacterId } from '../characters';
import api from '../../api';

let mock: MockAdapter;

beforeEach(() => {
  mock = new MockAdapter(api, { delayResponse: 4000 });
});
describe('tests call services', () => {
  it('should return array characters', async () => {
    const data = [
      {
        id: '1016823',
      },
    ];
    mock.onGet('characters?offset=0').reply(200, data);
    const response = await allCharacters(0);
    expect(data[0].id).toBe(response[0].id);
  });

  it('should return array characters', async () => {
    const data = {
      id: '1016823',
    };
    mock.onGet('characters/1016823').reply(200, data);
    const response = await allSeriesFromCharacterId('1016823');
    expect(data.id.toString()).toBe(response.id);
  });

  it('should return array series by characterId', async () => {
    const series = [{
      id: '13082',
    }];
    mock.onGet('characters/1009144/series').reply(200, series);
    const response = await allSeriesByCharacterId('1009144');
    expect(series[0].id).toBe(response[0].id);
  });
});
