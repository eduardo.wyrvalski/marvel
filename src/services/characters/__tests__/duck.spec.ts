import { Reducer } from 'redux-testkit';
import reducer, { charactersActions, initialState } from '../duck';

describe('test characters duck', () => {
  const characters = [
    {
      id: '1016823',
    },
  ];
  const characterId = '1016823';

  const stateCharacters = {
    characters,
    series: [],
    loading: false,
  };

  const stateCharacterInfo = {
    characterInfo: characterId,
    characters: [],
    series: [],
    loading: false,
  };

  const stateSeriesInfo = {
    characterInfo: undefined,
    characters: [],
    series: characterId,
    loading: false,

  };
  const page = 0;
  it('should load characters to system', () => {
    Reducer(reducer).expect(charactersActions.load({ page }));
  });
  it('should set characters to system', () => {
    Reducer(reducer)
      .expect(charactersActions.set(characters))
      .toReturnState(stateCharacters);
  });
  it('should loadById character to system', () => {
    Reducer(reducer).expect(charactersActions.loadById({ characterId }));
  });
  it('should setById character to system', () => {
    Reducer(reducer)
      .expect(charactersActions.setById(characterId))
      .toReturnState(stateCharacterInfo);
  });
  it('should loadById character to system', () => {
    Reducer(reducer).expect(charactersActions.loadSerieById({ characterId }));
  });
  it('should setById character to system', () => {
    Reducer(reducer)
      .expect(charactersActions.setSerieById(characterId))
      .toReturnState(stateSeriesInfo);
  });
  it('should reset to initial state', () => {
    Reducer(reducer)
      .expect(charactersActions.reset())
      .toReturnState(initialState);
  });
});
