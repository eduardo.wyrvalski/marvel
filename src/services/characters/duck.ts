import { createSlice, PayloadAction } from '@reduxjs/toolkit';

export const initialState = {
  characters: [],
  characterInfo: undefined,
  series: [],
  loading: false,
};

const allCharactersSlice = createSlice({
  name: 'characters',
  initialState,
  reducers: {
    load: (state, { payload }: PayloadAction<{ page: number }>) => ({
      ...state,
      payload,
    }),
    loadById: (state, { payload }: PayloadAction<{ characterId: string }>) => ({
      ...state,
      payload,
    }),
    loadSerieById: (
      state,
      { payload }: PayloadAction<{ characterId: string }>,
    ) => ({
      ...state,
      payload,
    }),
    setSerieById: (state, { payload }) => ({
      ...state,
      series: payload,
    }),
    setLoader: (state, { payload }) => ({
      ...state,
      loading: payload,
    }),
    setById: (state, { payload }) => ({ ...state, characterInfo: payload }),
    set: (state, { payload }) => ({ ...state, characters: payload }),
    reset: () => initialState,
  },
});

export const charactersActions = allCharactersSlice.actions;

export default allCharactersSlice.reducer;
