import { Character, Series } from '../interface';
import api from '../api';

export async function allCharacters(offset: number): Promise<Character[]> {
  const response = await api.get(`characters?offset=${offset}`);
  return response.data;
}

export const allSeriesFromCharacterId = async (
  characterId: string,
): Promise<Character> => {
  const response = await api.get(`characters/${characterId}`);
  return response.data;
};

export const allSeriesByCharacterId = async (characterId: string): Promise<Series[]> => {
  const response = await api.get(`characters/${characterId}/series`);
  return response.data;
};
