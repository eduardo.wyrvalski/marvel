import axios from 'axios';
import md5 from 'md5';

const publicKey = process?.env.REACT_APP_PUBLIC_KEY || '';
const privateKey = process.env.REACT_APP_PRIVATE_KEY || '';
const time = 1;

const hash = md5(time + privateKey + publicKey);

const api = axios.create({
  baseURL: process.env.REACT_APP_API_URL,
});

api.defaults.params = {};
api.defaults.params.ts = 1;
api.defaults.params.apikey = process.env.REACT_APP_PUBLIC_KEY;
api.defaults.params.hash = hash;

export default api;
