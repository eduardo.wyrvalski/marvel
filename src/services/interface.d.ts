interface thumbnail {
  path: string;
  extension: string;
}

export interface Character {
  id: number;
  name: string;
  thumbnail: thumbnail;
}

export interface Items {
  name: string
}

interface Series {
  id: string,
  available: number,
  title: string,
  thumbnail: thumbnail
}

export interface CharacterInfoDetails {
  id: number,
  name: string,
  description: string,
  series: Series
  thumbnail: thumbnail
}
