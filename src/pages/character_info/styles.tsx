import styled from 'styled-components';

export const CharacterInfoContainer = styled.section`
  display: flex;
  justify-content: space-around;
  padding: 10px;
  height: 400px;
  @media(max-width: 800px) {
    flex-direction: column;
    height: auto;
  }
`;

export const ImageContainer = styled.article`
  display: flex;
  padding: 10px;
  @media(max-width: 800px) {
    flex-direction: column;
    align-items: center;
    height: auto;
  }
`;

export const InfosContainer = styled.article`
  display: flex;
  justify-content: flex-start;
  align-items: center;
  padding: 5px;
  flex-direction: column;
  overflow: auto;
  width: 600px;
  nav {
    display: flex;
    flex-direction: row;
    flex-wrap: wrap;
    justify-content: space-between;
  }
`;

export const Title = styled.h1`
  width: 100%;
  font-size: 40px;
  display: flex;
  justify-content: center;
`;

export const Serie = styled.li`
  list-style-type: none;
  padding: 20px;
  width: 210px;
  h1 {
    font-size: 12px;
  }
`;

export const Button = styled.button`
  background-color: #FF6347;
  color: whitesmoke;
  border-radius: 5px;
  padding: 10px;
  margin: 10px 40px;
  width: 200px;
  :hover {
    cursor: pointer;
  }
`;

export const ButtonContainer = styled.div`
width: 100%;
  display: flex;
  justify-content: flex-end;
`;
