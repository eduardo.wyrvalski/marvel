import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { Appstate } from '../../store/rootReducer';
import { charactersActions } from '../../services/characters/duck';
import charactersSelector from '../../store/selector';
import {
  CharacterInfoContainer,
  ImageContainer,
  InfosContainer,
  Title,
  Serie,
  Button,
  ButtonContainer,
} from './styles';
import { Image } from '../../containers/card/styles';
import { CharacterInfoDetails, Series } from '../../services/interface';
import EditPage from '../edit_page';
import Loader from '../../containers/loader';

const CharacterInfo: React.FC = () => {
  const dispatch = useDispatch();
  const { id: characterId } = useParams<{ id: string }>();
  const [isOpen, setIsOpen] = useState<boolean>(false);
  const character = useSelector<Appstate, CharacterInfoDetails | undefined>(
    charactersSelector.getSelectedCharacter,
  );
  const loader = useSelector(charactersSelector.getLoader);
  const series = useSelector<Appstate, Series[] | []>(
    charactersSelector.getSeriesFromCharacterId,
  );

  const onCloseEditPage = () => {
    setIsOpen(!isOpen);
  };

  useEffect(() => {
    dispatch(charactersActions.loadById({ characterId }));
    dispatch(charactersActions.loadSerieById({ characterId }));
  }, []);
  return (
    <>
      {loader ? (
        <Loader />
      ) : (
        character && (
          <>
            <Title>{character.name}</Title>
            <CharacterInfoContainer>
              <ImageContainer>
                <Image
                  alt={character?.name}
                  widthProps="400px"
                  heightProps="400px"
                  src={`${character?.thumbnail?.path}.${character?.thumbnail?.extension}`}
                />
              </ImageContainer>
              <InfosContainer>
                <nav>
                  {series.map((serie) => (
                    <Serie>
                      <Image
                        alt={serie.title}
                        widthProps="200px"
                        heightProps="200px"
                        src={`${serie?.thumbnail?.path}.${serie?.thumbnail?.extension}`}
                      />
                      <h1>{serie.title}</h1>
                    </Serie>
                  ))}
                </nav>
              </InfosContainer>
            </CharacterInfoContainer>
            <ButtonContainer>
              <Button onClick={() => setIsOpen(!isOpen)} type="button">
                Editar
              </Button>
            </ButtonContainer>
            {isOpen && (
              <EditPage onClose={onCloseEditPage} character={character} />
            )}
          </>
        )
      )}
    </>
  );
};

export default CharacterInfo;
