import styled from 'styled-components';

const ListContainer = styled.div`
  position: relative;
  display: grid;
  grid-template-columns: repeat(auto-fill, 250px);
  flex-wrap: wrap;
  width: 100%;
  justify-content: space-around;
  @media(max-width: 520px) {
    justify-content: center;
  }

`;

export default ListContainer;
