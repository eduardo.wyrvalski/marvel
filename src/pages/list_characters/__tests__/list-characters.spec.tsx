import 'jsdom-global/register';
import React from 'react';
import { mount } from 'enzyme';
import ListCharacters from '../index';
import mockProvider from '../../../../hoc/mock-provider';

describe('tests list characters', () => {
  const characters = [
    {
      name: 'eduardo',
      thumbnail: { path: 'teste', extension: 'jpg' },
    },
  ];

  it('render app', () => {
    const ComponentWithStore = mockProvider(ListCharacters, {
      characters: { characters },
    });
    const wrapper = mount(<ComponentWithStore />);
    expect(wrapper).toMatchSnapshot();
  });
});
