import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Pagination } from '@material-ui/lab';

import Loader from '../../containers/loader';
import { charactersActions } from '../../services/characters/duck';
import { Appstate } from '../../store/rootReducer';
import Card from '../../containers/card';
import { Character } from '../../services/interface';
import ListContainer from './styles';
import charactersSelector from '../../store/selector';

const ListCharacters: React.FC = () => {
  const dispatch = useDispatch();
  const [page, setPage] = useState<number>(1);
  const characters: Character[] = useSelector(
    (state: Appstate) => state?.characters?.characters,
  );
  const loader = useSelector(charactersSelector.getLoader);
  useEffect(() => {
    dispatch(charactersActions.load({ page: (page - 1) * 20 }));
  }, [page]);
  const changePage = (event: unknown, value: number) => {
    setPage(value);
  };

  return (
    <>
      <Pagination
        className="pagination"
        count={78}
        page={page}
        onChange={changePage}
      />
      {loader ? (
        <Loader />
      ) : (
        <ListContainer>
          {characters.map((character: Character) => (
            <Card
              key={`${character.id} ${character.name}`}
              id={character.id}
              name={character.name}
              thumbnail={character.thumbnail}
            />
          ))}
        </ListContainer>
      )}
      <Pagination
        className="pagination"
        count={78}
        page={page}
        onChange={changePage}
      />
    </>
  );
};

export default ListCharacters;
