import { mount } from 'enzyme';
import React from 'react';
import configureStore from 'redux-mock-store';
import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';
import EditPage from '../index';

describe('tests edit page', () => {
  const character = {
    id: 1,
    name: 'eduardo',
    thumbnail: { path: 'teste', extension: 'jpg' },
  };

  const mockStore = configureStore([]);

  const store = mockStore({
    character: { character },
  });

  const wrapper = mount(
    <Provider store={store}>
      <BrowserRouter>
        <EditPage character={character} onClose={jest.fn()} />
      </BrowserRouter>
    </Provider>,
  );
  it('render edit page', () => {
    expect(wrapper).toMatchSnapshot();
  });
  it('submit form', () => {
    wrapper.find('form').simulate('submit');
    expect(store.getActions()).toBeTruthy();
  });
  it('close form', () => {
    wrapper.find('#close-button').at(0).simulate('click');
    expect(store.getActions()).toBeTruthy();
  });
});
