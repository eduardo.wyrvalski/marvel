import React, { ReactElement } from 'react';
import { useForm } from 'react-hook-form';
import { useDispatch } from 'react-redux';
import {
  EditPageContainer,
  Container,
  CloseButton,
  EditInfosContainer,
} from './styles';
import { Character } from '../../services/interface';
import { Image } from '../../containers/card/styles';
import { Button, ImageContainer } from '../character_info/styles';
import Input from '../../containers/Input';
import { charactersActions } from '../../services/characters/duck';

interface EditPageProps {
  onClose: () => void;
  character: Character;
}

const EditPage = ({ onClose, character }: EditPageProps): ReactElement => {
  const { register, handleSubmit } = useForm();
  const dispatch = useDispatch();
  const onSubmit = handleSubmit((data) => {
    const extension = data.image.substr(data.image.lastIndexOf('.') + 1);
    const image = data.image.substr(0, data.image.lastIndexOf('.'));
    const newInfo = {
      ...character,
      name: data.name,
      thumbnail: { path: image, extension },
    };
    dispatch(charactersActions.setById(newInfo));
  });
  return (
    <EditPageContainer>
      <CloseButton id="close-button" onClick={() => onClose()} />
      <Container>
        <ImageContainer>
          <Image
            src={`${character.thumbnail.path}.${character.thumbnail.extension}`}
            alt={character.name}
            widthProps="400px"
            heightProps="400px"
          />
        </ImageContainer>
        <EditInfosContainer>
          <h1>{character.name}</h1>
          <form onSubmit={onSubmit}>
            <Input label="Nome" type="text" register={register('name')} />
            <Input
              type="text"
              label="URL da imagem"
              register={register('image')}
            />
            <div id="container-button">
              <Button type="submit"> Enviar</Button>
            </div>
          </form>
        </EditInfosContainer>
      </Container>
    </EditPageContainer>
  );
};

export default EditPage;
