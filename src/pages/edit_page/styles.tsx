import styled from 'styled-components';

export const EditPageContainer = styled.div`
  background-color: rgb(0, 0, 0, 0.8);
  display: flex;
  z-index: 2;
  width: 100%;
  height: 100%;
  justify-content: center;
  align-items: center;
  position: fixed;
  left: 0;
  bottom: 0;
`;

export const Container = styled.div`
  background-color: #D0C3C3FF;
  position: fixed;
  width: 896px;
  height: 418px;
  padding: 10px;
  display: flex;
  border-radius: 10px;
`;

export const CloseButton = styled.div`
  position: absolute;
  cursor: pointer;
  top: calc(100vh / 2 - (418px / 2) - 50px);
  right: calc(100vw / 2 - (896px / 2));
  width: 32px;
  height: 32px;
  opacity: 1;
  
  :before,
  :after {
    position: absolute;
    left: 15px;
    content: ' ';
    height: 33px;
    width: 2px;
    background-color: white;
  }
  :before {
    transform: rotate(45deg);
  }
  :after {
    transform: rotate(-45deg);
  }
`;

export const EditInfosContainer = styled.article`
  display: flex;
  align-items: center;
  flex-direction: column;
  justify-content: center;
  width: 100%;
  form {
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    height: 200px;
    #container-button {
      display: flex;
      width: 100%;
      justify-content: center;
      button {
        margin: 0;
      }
    }
  }
`;
