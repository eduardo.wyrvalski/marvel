import React from 'react';
import { Router, Switch, Route, BrowserRouter } from 'react-router-dom';
import Header from './containers/header';
import ListCharacters from './pages/list_characters';
import Main from './styles';
import history from './store/history';
import CharacterInfo from './pages/character_info';

function Routes(): JSX.Element {
  return (
    <Router history={history}>
      <BrowserRouter>
        <Header />
        <Main>
          <Switch>
            <Route path="/" exact component={ListCharacters} />
            <Route path="/character/info/:id" component={CharacterInfo} />
          </Switch>
        </Main>
      </BrowserRouter>
    </Router>
  );
}

export default Routes;
